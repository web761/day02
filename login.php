<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div id="main">

        <div class="wrapper">
            <div class="time">
                <p>Bây giờ là <?php
                                date_default_timezone_set('Asia/Ho_Chi_Minh');
                                echo date("G:i") ?> <?php echo date("d/m/Y") ?>
                </p>
            </div>
            <form action="" method="post">
                <?php echo '<div class="form-group">
                    <label class="form-label">Tên đăng nhập</label>
                    <input type="text" name="username" id="">
                    
                </div>' ?>
                <?php echo '<div class="form-group">
                    <label class="form-label">Mật khẩu</label>
                    <input type="password" name="password" id="">
                    
                </div>' ?>

                <button class="form-button">Đăng nhập</button>
            </form>
        </div>
    </div>
</body>

</html>
<!--   -->